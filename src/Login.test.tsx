import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Login, { parseUserId } from "./Login";
import * as sdk from "matrix-js-sdk";

// high timeout because client config discovery timeout is slow
jest.setTimeout(20000);

test("Parse @alice:example.com", () => {
  let parsed = parseUserId("@alice:example.com");
  expect(parsed).toEqual({ localpart: "alice", servername: "example.com" });
});

test("Parse some weird stuff", () => {
  var parsed: undefined | object;

  parsed = parseUserId("alice:example.com");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("alice");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("@alice");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("@alice:");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("@:example.com");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("alice@example.com");
  expect(parsed).toEqual(undefined);

  parsed = parseUserId("@alice@example.com");
  expect(parsed).toEqual(undefined);
});

test("Log in as guest", async () => {
  let setClient = jest.fn();
  render(<Login setClient={setClient} />);

  // click "guest"
  let guestBtn = screen.getByRole("guestlogin");
  userEvent.click(guestBtn);

  await waitFor(() => expect(setClient).toHaveBeenCalledTimes(1));

  // check that we got a `MatrixClient` back
  let client: sdk.MatrixClient | undefined = setClient.mock.calls[0][0];
  expect(client && client.isGuest()).toEqual(true);
});

test("Wrong username error", async () => {
  render(<Login setClient={() => undefined} />);
  const userIdField = screen.getByRole("userid");
  const passwordField = screen.getByRole("password");
  const loginBtn = screen.getByRole("login");

  userEvent.clear(userIdField);
  userEvent.clear(passwordField);
  userEvent.type(userIdField, "@alice:example.com");
  userEvent.type(passwordField, "123");
  userEvent.click(loginBtn);
  expect(
    await screen.findByRole("homeserverurl", {}, { timeout: 10000 })
  ).toBeInTheDocument();
});

test("Wrong password error", async () => {
  render(<Login setClient={() => undefined} />);
  const userIdField = screen.getByRole("userid");
  const passwordField = screen.getByRole("password");
  const loginBtn = screen.getByRole("login");

  userEvent.clear(userIdField);
  userEvent.clear(passwordField);
  userEvent.type(userIdField, "@dev:localhost:8008");
  userEvent.type(passwordField, "notthepassword");
  userEvent.click(loginBtn);

  // wait for prompt to ask for homeserver url
  // (since .well-known always fails on localhost homeserver)
  const homeserverUrlField = await screen.findByRole(
    "homeserverurl",
    {},
    { timeout: 15000 }
  );
  userEvent.type(homeserverUrlField, "http://localhost:8008");
  userEvent.click(loginBtn);

  // expect login to fail
  expect(await screen.findByText(/M_FORBIDDEN/i)).toBeInTheDocument();
});
