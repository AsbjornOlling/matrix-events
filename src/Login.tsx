import { useState, useEffect } from "react";
import * as sdk from "matrix-js-sdk";
import { MatrixError } from "matrix-js-sdk/lib/http-api";
import { useNavigate } from "react-router-dom";
import {
  Button,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Heading,
  HStack,
  Box,
  Center,
} from "@chakra-ui/react";

// compile time configuration constant
const GUEST_HOMESERVER_URL: string =
  process.env.REACT_APP_GUEST_HOMESERVER_URL || "http://localhost:8008";

type UserId = { localpart: string; servername: string };

export function parseUserId(userIdInput: string): UserId | undefined {
  /**
   * Parses a userid strings into localpart and servername.
   * Returns `undefined` if parsing fails.
   * @param {string} userIdInput - User input, an un-parsed userid string.
   * @returns {UserId | undefined} - Parsed result, `undefined` if parse fails.
   */
  if (!userIdInput.startsWith("@")) return undefined;
  if (!userIdInput.includes(":")) return undefined;
  const localpart: string = userIdInput.split(":")[0].slice(1);
  const servername: string = userIdInput.split(":").slice(1).join(":");
  if (!localpart || !servername) return undefined;

  // parse success!
  return { localpart: localpart, servername: servername };
}

export async function loginWithUserId(
  userId: UserId,
  password: string,
  homeserverUrl?: string
): Promise<sdk.MatrixClient> {
  /**
   * Log in with userid and password. Optionally provide a homeserver url.
   * @params {UserId} object with `localpart` and `servername`
   * @params {string} password to log in with
   * @params {string} homeserver url to use. If not provided, the homeserver
   * url is looked up using the .well-known path.
   */

  async function lookupHomeserver(): Promise<string> {
    // look up homeserver base url from userid servername
    const clientConfig: sdk.IClientWellKnown =
      await sdk.AutoDiscovery.findClientConfig(userId.servername);
    const hsUrl: string | null | undefined =
      clientConfig?.["m.homeserver"]?.base_url;
    if (!hsUrl)
      throw new Error("No homeserver url found in .well-known response");
    return hsUrl;
  }

  // look up homeserver url if not provided
  const hsUrl: string = homeserverUrl
    ? homeserverUrl
    : await lookupHomeserver();

  // make a client and log in
  const client: sdk.MatrixClient = sdk.createClient(hsUrl);

  await client.loginWithPassword(userId.localpart, password);

  // lets go
  await client.startClient({ disablePresence: true });

  // done!
  return client;
}

interface LoginProps {
  setClient: (c: sdk.MatrixClient) => void;
}

function Login(props: LoginProps) {
  // user login state
  const [userIdInput, setUserIdInput] = useState("");
  const [password, setPassword] = useState("");
  const [userId, setUserId] = useState<UserId | undefined>();
  const [homeserverUrl, setHomeserverUrl] = useState<string | undefined>();
  const [loginError, setLoginError] = useState<MatrixError | undefined>();
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  console.log("error:");
  console.log(loginError);

  // parse and set user id on user input
  useEffect(() => {
    let parsed: UserId | undefined = parseUserId(userIdInput);
    setUserId(parsed);
  }, [userIdInput]);

  function registerGuest() {
    const tmpClient = sdk.createClient(GUEST_HOMESERVER_URL);
    tmpClient
      .registerGuest({})
      .then((e) => {
        const client = sdk.createClient({
          baseUrl: GUEST_HOMESERVER_URL,
          accessToken: e.access_token,
          userId: e.user_id,
          deviceId: e.device_id,
        });
        client.setGuest(true);
        props.setClient(client);
      })
      .catch((e) => {
        // TODO: display error on page
        console.log(e.errcode + ": " + e.message);
      });
  }

  const loginFun = (
    userId: UserId,
    password: string,
    homeserverUrl?: string
  ) => {
    // log in with data from form, set client on success
    // enable homeserver url field if server lookup fails
    (async () => setIsLoading(true))()
      .then(() => loginWithUserId(userId, password, homeserverUrl))
      .then((c) => {
        props.setClient(c);
        navigate("/create");
      })
      .catch((e) => {
        if (e.message.toLowerCase().includes("homeserver url")) {
          // failed looking up homeserver url from user id,
          // enable homesever url field
          setHomeserverUrl("");
          setLoginError(e);
        } else {
          // something went wrong. let the user know.
          setLoginError(e);
        }
        setIsLoading(false);
      });
  };

  const homeserverUrlValid: boolean = new RegExp("^https?://").test(
    homeserverUrl || ""
  );
  return (
    <Center mt={200}>
      <Box>
        <Heading size="md">Login</Heading>

        <FormControl mt={5} isInvalid={!userId && userIdInput !== ""}>
          <FormLabel htmlFor="userid">User ID</FormLabel>
          <Input
            id="userid"
            role="userid"
            type="text"
            value={userIdInput}
            placeholder="@user:example.com"
            onChange={(e) => setUserIdInput(e.target.value)}
          />
          <FormErrorMessage>
            Your user ID should be in the form @user:example.com.
          </FormErrorMessage>
        </FormControl>

        <FormControl mt={5}>
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input
            id="password"
            role="password"
            type="password"
            value={password}
            placeholder="●●●●●●●●●●●●●●●●●"
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormControl>

        {homeserverUrl !== undefined ? (
          <FormControl
            isRequired
            isInvalid={homeserverUrl !== "" && !homeserverUrlValid}
          >
            <FormLabel htmlFor="homeserverurl">Homeserver URL</FormLabel>
            <Input
              id="homeserverurl"
              role="homeserverurl"
              type="url"
              value={homeserverUrl}
              placeholder="https://example.com:8448"
              onChange={(e) => setHomeserverUrl(e.target.value)}
            />
            <FormHelperText>The URL of your Matrix homeserver.</FormHelperText>
            <FormErrorMessage>
              Your homeserver URL should start with "https://".
            </FormErrorMessage>
          </FormControl>
        ) : (
          ""
        )}

        <HStack mt={5}>
          <Button
            role="login"
            isLoading={isLoading}
            type="submit"
            onClick={() =>
              userId ? loginFun(userId, password, homeserverUrl) : ""
            }
          >
            Log in
          </Button>

          <Button role="guestlogin" onClick={registerGuest}>
            Log in as guest
          </Button>
        </HStack>

        {loginError ? (
          <p>
            {loginError.name}: {loginError.message}
          </p>
        ) : (
          ""
        )}
      </Box>
    </Center>
  );
}

export default Login;
