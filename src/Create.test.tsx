import { render, screen } from "@testing-library/react";
import * as sdk from "matrix-js-sdk";
import * as crypto from "crypto";
import { createTestMatrixClient } from "./setupTests";
import { CustomEventType } from "./customEventTypes";
import userEvent from "@testing-library/user-event";

// the module we're testing
import Create, { createEventRoom } from "./Create";

test("Create page renders", async () => {
  let setClient = jest.fn();
  let testClient = await createTestMatrixClient();
  render(<Create client={testClient} setClient={setClient} />);
  expect(screen.getByText(/Create an event/i)).toBeInTheDocument();
});

test("Create event without image", async () => {
  let testClient = await createTestMatrixClient();

  // create event room
  const start_date = new Date("2/2/22");
  const end_date = new Date("3/3/33");
  const { roomId, eventId } = await createEventRoom(testClient, {
    title: crypto.randomUUID(),
    topic: "Just a test event",
    description: "woah what a test event",
    image: undefined,
    start: start_date,
    end: end_date,
    location: {
      description: "Your moms place",
      geo_uri: { lat: "69.69", lon: "4.20" },
    },
  });

  // get pinned messages
  const pinnedState = await testClient.getStateEvent(
    roomId,
    sdk.EventType.RoomPinnedEvents,
    ""
  );

  // count 'em
  expect(pinnedState.pinned.length).toEqual(1);

  const calendarEvent = await testClient.fetchRoomEvent(
    roomId,
    pinnedState.pinned[0]
  );
  expect(calendarEvent.type).toEqual(CustomEventType.CalendarEvent);

  // check room type
  const createEvent = await testClient.getStateEvent(
    roomId,
    "m.room.create",
    ""
  );
  expect(createEvent.type).toEqual(CustomEventType.CalendarEventRoom);
});

test("Create event with image", async () => {
  const testClient = await createTestMatrixClient();

  // create event room
  const start_date = new Date("2/2/22");
  const end_date = new Date("3/3/33");
  const fakepng: File = new File(["foobar"], "image.png", {
    type: "image/png",
  });
  const { roomId, eventId } = await createEventRoom(testClient, {
    title: crypto.randomUUID(),
    topic: "Just a test event",
    description: "woah what a test event",
    image: fakepng,
    start: start_date,
    end: end_date,
    location: {
      description: "Your moms place",
      geo_uri: { lat: "69.69", lon: "4.20" },
    },
  });

  // check that there is an image in the room
  const avatarEvent = await testClient.getStateEvent(
    roomId,
    "m.room.avatar",
    ""
  );
  expect(typeof avatarEvent.url).toEqual("string");
  // expect url like "mxc://localhost:8008/FoObAr
  expect(avatarEvent.url).toMatch(/^mxc:\/\/localhost:8008\/[A-Za-z]+/i);
});
