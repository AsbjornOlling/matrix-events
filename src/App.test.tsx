import { render, screen } from "@testing-library/react";
import App, { saveClient, loadClient } from "./App";
import { createTestMatrixClient } from "./setupTests";
import * as sdk from "matrix-js-sdk";

test("Default page shows navbar", () => {
  render(<App />);
  const linkElement = screen.getByRole("navbar");
  expect(linkElement).toBeInTheDocument();
});

test("Save and load test client in localstorage", async () => {
  // make test client
  const testClient: sdk.MatrixClient = await createTestMatrixClient();
  // save it
  saveClient(testClient);
  // load it
  const loadedClient: sdk.MatrixClient | undefined = await loadClient();
  expect(loadedClient).toBeTruthy();
  if (!loadedClient) return;

  // check that they're the same
  expect(loadedClient.isGuest()).toEqual(testClient.isGuest());
  expect(loadedClient.isLoggedIn()).toEqual(testClient.isLoggedIn());
  expect(loadedClient.getUserId()).toEqual(testClient.getUserId());
  expect(loadedClient.getAccessToken()).toEqual(testClient.getAccessToken());
});
