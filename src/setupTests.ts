// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom";
import * as sdk from "matrix-js-sdk";
import { logger } from "matrix-js-sdk/lib/logger";
import log from "loglevel";
import * as crypto from "crypto";
import { loginWithUserId } from "./Login";

export const mockedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedNavigate,
}));

logger.setLevel(log.levels.SILENT, false);

export async function createTestMatrixClient(): Promise<sdk.MatrixClient> {
  // wait for homeserver to come up
  var tmpClient: sdk.MatrixClient;
  while (true) {
    try {
      tmpClient = sdk.createClient("http://localhost:8008");
      break;
    } catch (e) {
      console.log("Waiting for synapse...");
      continue;
    }
  }
  let username: string = crypto.randomUUID();
  await tmpClient.register(username, username, null, { type: "m.login.dummy" });

  // login as testbob
  let testClient: sdk.MatrixClient = await loginWithUserId(
    { localpart: username, servername: "localhost:8008" },
    username,
    "http://localhost:8008"
  );
  return testClient;
}
