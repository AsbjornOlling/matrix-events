import { render, screen } from "@testing-library/react";
import * as crypto from "crypto";
import * as sdk from "matrix-js-sdk";
import { createMemoryHistory } from "history";
import { createTestMatrixClient } from "./setupTests";
import { createEventRoom, ViewEventRoomParams } from "./Create";
import { MemoryRouter } from "react-router-dom";

// the module we're testing
import View from "./View";

// because shit is slow
jest.setTimeout(20000);

async function createTestCalendarEventRoom(
  c: sdk.MatrixClient
): Promise<ViewEventRoomParams> {
  const start_date = new Date("2/2/22");
  const end_date = new Date("3/3/33");
  return await createEventRoom(c, {
    title: "Test Party",
    topic: "Just a test event",
    description: "woah what a test event",
    image: undefined,
    start: start_date,
    end: end_date,
    location: {
      description: "Your moms place",
      geo_uri: { lat: "69.69", lon: "4.20" },
    },
  });
}

test("View page renders", async () => {
  const testClient = await createTestMatrixClient();
  const { roomId, eventId } = await createTestCalendarEventRoom(testClient);

  render(
    <MemoryRouter initialEntries={[`/event/${roomId}/${eventId}`]}>
      <View client={testClient} roomId={roomId} eventId={eventId} />
    </MemoryRouter>
  );
  expect(
    await screen.findByText("Test Party", {}, { timeout: 10000 })
  ).toBeInTheDocument();
});
