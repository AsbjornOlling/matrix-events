import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { MemoryRouter } from "react-router-dom";
import { createTestMatrixClient } from "./setupTests";

// module we're testing
import { Nav } from "./Nav";

test("Logout user", async () => {
  let setClient = jest.fn();
  const testClient = await createTestMatrixClient();
  const { rerender } = render(
    <MemoryRouter>
      <Nav client={testClient} setClient={setClient} />
    </MemoryRouter>
  );
  expect(await screen.findByText(/Logout/i)).toBeInTheDocument();
  expect(await screen.findByTestId("displayname")).toBeInTheDocument();
  const logoutBtn = screen.getByText("Logout");
  userEvent.click(logoutBtn);
  expect(!testClient.isLoggedIn()).toEqual(false);

  rerender(
    <MemoryRouter>
      <Nav client={undefined} setClient={setClient} />
    </MemoryRouter>
  );

  testClient.stopClient();
  await waitFor(() => {
    expect(screen.queryByTestId("displayname")).not.toBeInTheDocument();
  });
});
