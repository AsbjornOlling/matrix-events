export enum CustomEventType {
  CalendarEventRoom = "chat.cactus.calendar.room",
  CalendarEvent = "chat.cactus.calendar.event",
  CalendarEventAttendancePoll = "sh.pieceof.calendar.event.attendance_poll",
  CalendarEventTime = "sh.pieceof.calendar.event.time",
  CalendarEventLocation = "sh.pieceof.calendar.event.location",
}

export enum CalendarEventTimeKey {
  start = "start",
  stop = "stop",
}
