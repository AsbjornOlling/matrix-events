import React, { HTMLAttributes } from "react";
import { useColorMode } from "@chakra-ui/react";
import Geolookup from "react-geolookup-v2";
import "./geoLookup.css";

interface Props {
  inputClassName: string;
  disableAutoLookup: boolean;
  onSuggestsLookup: Function;
  onGeocodeSuggest: Function;
  getSuggestLabel: Function;
  onSuggestSelect: Function;
  onSuggestNoResults: Function;
  radius: string;
}

const GeoLookup = ({
  disableAutoLookup = true,
  onSuggestsLookup,
  onGeocodeSuggest,
  getSuggestLabel,
  onSuggestSelect,
  onSuggestNoResults,
  radius,
  ...props
}: Props & HTMLAttributes<HTMLElement>) => {
  const isLight = useColorMode().colorMode === "light"; //you can check what theme you are using right now however you want
  return (
    <div className={isLight ? "light-theme" : "dark-theme"}>
      <Geolookup
        disableAutoLookup={true}
        onSuggestsLookup={onSuggestsLookup}
        onGeocodeSuggest={onGeocodeSuggest}
        getSuggestLabel={getSuggestLabel}
        onSuggestSelect={onSuggestSelect}
        onSuggestNoResults={onSuggestNoResults}
        radius="20"
        inputClassName="geolookup__input--nominatim"
      />
    </div>
  );
};

export default GeoLookup;
