import { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Avatar,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuDivider,
  useColorModeValue,
  Stack,
  useColorMode,
  Center,
} from "@chakra-ui/react";
import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import * as sdk from "matrix-js-sdk";
import { useNavigate } from "react-router";

async function getUserData(
  c: sdk.MatrixClient | undefined
): Promise<{ avatar_url?: string; displayname: string } | undefined> {
  if (!c) return;

  const userId: string | undefined = c.getUserId();
  if (!userId) return;

  const userdata: { avatar_url?: string; displayname?: string } | undefined =
    (await c.getProfileInfo(userId)) as object;

  const httpAvatarUrl: string | undefined = userdata.avatar_url
    ? c.mxcUrlToHttp(userdata.avatar_url) || undefined
    : undefined;

  return {
    displayname: userdata.displayname || userId,
    avatar_url: httpAvatarUrl,
  };
}

interface NavProps {
  client: sdk.MatrixClient | undefined;
  setClient: (c: sdk.MatrixClient | undefined) => void;
}

export function Nav(props: NavProps) {
  const { colorMode, toggleColorMode } = useColorMode();
  const [avatarUrl, setAvatarUrl] = useState<string | undefined>(undefined);
  const [displayname, setDisplayname] = useState<string>("");
  const navigate = useNavigate();

  // fetch and set user account data
  useEffect(() => {
    if (!props.client) {
      setAvatarUrl(undefined);
      setDisplayname("");
      return;
    }
    getUserData(props.client).then((data) => {
      if (!data) return;
      if (data.avatar_url) setAvatarUrl(data.avatar_url);
      if (data.displayname) setDisplayname(data.displayname);
    });
  }, [props.client]);

  return (
    <Box bg={useColorModeValue("gray.100", "gray.900")} px={4} role="navbar">
      <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
        <Box>Logo</Box>

        <Flex alignItems={"center"}>
          <Stack direction={"row"} spacing={7}>
            <Button onClick={toggleColorMode}>
              {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
            </Button>

            <Menu>
              <MenuButton
                as={Button}
                rounded={"full"}
                variant={"link"}
                cursor={"pointer"}
                minW={0}
              >
                <Avatar size={"sm"} src={avatarUrl} />
              </MenuButton>
              <MenuList alignItems={"center"}>
                <br />
                <Center>
                  <Avatar size={"2xl"} src={avatarUrl} />
                </Center>
                <br />
                {displayname ? (
                  <Center>
                    <p data-testid="displayname">{displayname}</p>
                  </Center>
                ) : (
                  ""
                )}
                <br />
                <MenuDivider />
                {props.client ? (
                  <MenuItem onClick={() => props.setClient(undefined)}>
                    Logout
                  </MenuItem>
                ) : (
                  <MenuItem onClick={() => navigate("/login")}>Login</MenuItem>
                )}
              </MenuList>
            </Menu>
          </Stack>
        </Flex>
      </Flex>
    </Box>
  );
}
