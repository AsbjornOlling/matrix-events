import { useState } from "react";
import * as sdk from "matrix-js-sdk";
import { Visibility } from "matrix-js-sdk/lib/@types/partials";
import { EventType } from "matrix-js-sdk/lib/@types/event";
import { MatrixError } from "matrix-js-sdk/lib/http-api";
import "react-datepicker/dist/react-datepicker.css";
import { CustomEventType } from "./customEventTypes";
import { useNavigate } from "react-router-dom";
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Center,
  Box,
  Button,
  CloseButton,
  FormControl,
  FormHelperText,
  FormLabel,
  Heading,
  Input,
  Textarea,
} from "@chakra-ui/react";
import DatePicker from "./DatePicker";
import GeoLookup from "./GeoLookup";
import * as Nominatim from "nominatim-browser";

interface CreateEventRoomProps {
  title: string;
  aliasName?: string;
  topic: string;
  description: string;
  image: File | undefined;
  start: Date;
  end: Date;
  location: MatrixLocation | undefined;
}

interface GeoSuggetion {
  label: String;
  display_name: String;
  isFixture: Boolean;
  placeId: String;
  raw: { lat: String; lon: String; display_name: String };
  location: { lat: String; lon: String };
}

interface MatrixLocation {
  geo_uri: { lat: String; lon: String };
  description: String;
}

export interface ViewEventRoomParams {
  roomId: string;
  eventId: string;
}

export async function createEventRoom(
  c: sdk.MatrixClient,
  props: CreateEventRoomProps
): Promise<ViewEventRoomParams> {
  // create a Matrix Room and insert the details
  // returns a room id

  // Create the room
  const roomId: string = (
    await c.createRoom({
      room_alias_name: props.aliasName,
      invite: [],
      visibility: Visibility.Public,
      name: props.title,
      topic: props.description,
      creation_content: { type: CustomEventType.CalendarEventRoom },
    })
  ).room_id;

  const start_repr = props.start.toLocaleString();
  const end_repr = props.end.toLocaleString();
  const markdown_body = `# ${props.title}\n${props.description}\nLocation: ${props.location}\nStarts: ${start_repr}\nEnds: ${end_repr}`;
  const html_body = `# ${props.title}\n${props.description}\nLocation: ${props.location}\nStarts: ${start_repr}\nEnds: ${end_repr}`;
  // Send chat.cactus.calendar.event
  const content = {
    [CustomEventType.CalendarEvent]: {
      title: props.title,
      description: props.description,
      location: props.location,
      start: props.start.valueOf(),
      end: props.end.valueOf(),
    },
    "m.message": [
      {
        mimetype: "text/markdown",
        body: markdown_body,
      },
      {
        mimetype: "text/html",
        body: html_body,
      },
    ],
    "m.location": [
      {
        uri:
          "geo:" +
          props.location?.geo_uri.lat +
          "," +
          props.location?.geo_uri.lon,
        description: props.location?.description,
      },
    ],
    // TODO: m.image
  };
  const calendarEventId: string = (
    await c.sendEvent(roomId, CustomEventType.CalendarEvent, content)
  ).event_id;

  // pin all the sent events
  await c.sendStateEvent(
    roomId,
    EventType.RoomPinnedEvents,
    { pinned: [calendarEventId] },
    "" // empty state key
  );

  // Send room image
  if (props.image) await setRoomAvatar(c, roomId, props.image);

  return { roomId, eventId: calendarEventId };
}

async function setRoomAvatar(
  c: sdk.MatrixClient,
  roomId: string,
  image: File
): Promise<string> {
  const mxcUri = await c.uploadContent(image, { onlyContentUri: true });
  const r = await c.sendStateEvent(
    roomId,
    EventType.RoomAvatar,
    { url: mxcUri },
    ""
  );
  return r.event_id;
}

interface CreateProps {
  client: sdk.MatrixClient;
  setClient: (c: sdk.MatrixClient | undefined) => void;
}

function Create(props: CreateProps) {
  const client = props.client;
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [location, setLocation] = useState<MatrixLocation | undefined>();
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>(new Date());
  const [image, setImage] = useState<File | undefined>();
  const [creationError, setCreationError] = useState<MatrixError | undefined>();
  const [isCreating, setIsCreating] = useState(false);
  const navigate = useNavigate();

  const clickCreate = () => {
    // run this when clicking the "create" button
    setIsCreating(true);
    createEventRoom(client, {
      title: title,
      aliasName: title.replace(/\s+/g, "_"),
      topic: description,
      description: description,
      image: image,
      start: startDate,
      end: endDate,
      location: location,
    })
      .then(({ roomId, eventId }) => navigate(`/event/${roomId}/${eventId}`))
      .catch((e) => {
        setCreationError(e);
        setIsCreating(false);
      });
  };

  function onSuggestsLookup(userInput: string) {
    return Nominatim.geocode({ q: userInput, addressdetails: true });
  }

  function onGeocodeSuggest(suggest: GeoSuggetion) {
    if (suggest) {
      return {
        nominatim: suggest.raw || {},
        location: {
          lat: suggest.raw ? suggest.raw.lat : "",
          lon: suggest.raw ? suggest.raw.lon : "",
        },
        placeId: suggest.placeId,
        isFixture: suggest.isFixture,
        label: suggest.raw ? suggest.raw.display_name : "",
      };
    } else {
      return {};
    }
  }

  function getSuggestLabel(suggest: GeoSuggetion) {
    return suggest.display_name;
  }

  function onSuggestSelect(suggest: GeoSuggetion) {
    setLocation({
      description: suggest.label,
      geo_uri: suggest.location,
    });
    console.log("Label:" + suggest.label);
    console.log("placeId:" + suggest.placeId);
    console.log("location:" + suggest.location);
  }

  function onSuggestNoResults(userInput: string) {
    setLocation({
      description: userInput,
      geo_uri: { lat: "0.00", lon: "0.00" },
    });
  }

  return (
    <Box mt={16} mx={[4, 16, 64]}>
      <Heading size="2xl"> Create an event</Heading>

      <FormControl mt={10}>
        <FormLabel size="md" htmlFor="title">
          Title
        </FormLabel>
        <Input
          id="title"
          type="text"
          value={title}
          disabled={isCreating}
          onChange={(e) => setTitle(e.target.value)}
        />
        <FormHelperText>The title of your event.</FormHelperText>
      </FormControl>

      <FormControl mt={10}>
        <FormLabel size="md" htmlFor="description">
          Description
        </FormLabel>
        <Textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          disabled={isCreating}
        />
        <FormHelperText>
          Describe your event. Markdown not supported
        </FormHelperText>
      </FormControl>

      <FormControl mt={10}>
        <FormLabel size="md" htmlFor="startDate">
          Start time
        </FormLabel>
        <DatePicker
          id="startDate"
          selectedDate={startDate}
          onChange={(date: any) => setStartDate(date)}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={15}
          timeCaption="time"
          disabled={isCreating}
          dateFormat="yyyy/MM/dd HH:mm"
        />
        <FormHelperText>
          The start time of your event in your local timezone.
        </FormHelperText>
      </FormControl>

      <FormControl mt={10}>
        <FormLabel size="md" htmlFor="endDate">
          End time
        </FormLabel>
        <DatePicker
          id="endDate"
          selectedDate={endDate}
          onChange={(date: any) => setEndDate(date)}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={15}
          timeCaption="time"
          disabled={isCreating}
          dateFormat="yyyy/MM/dd HH:mm"
        />
        <FormHelperText>
          The end time of your event in your local timezone.
        </FormHelperText>
      </FormControl>

      <FormControl mt={10}>
        <FormLabel size="md" htmlFor="image">
          Image
        </FormLabel>
        <Input
          id="image"
          type="file"
          accept="image/*"
          disabled={isCreating}
          onChange={(e) =>
            setImage(e.target.files ? e.target.files[0] : undefined)
          }
        />
        <FormHelperText>
          (Optional) An image to be shown at the top of your event page.
        </FormHelperText>
      </FormControl>

      <FormControl mt={10}>
        <FormLabel size="md" htmlFor="image">
          Location
        </FormLabel>
        <GeoLookup
          inputClassName="geolookup__input--nominatim"
          disableAutoLookup={true}
          onSuggestsLookup={onSuggestsLookup}
          onGeocodeSuggest={onGeocodeSuggest}
          getSuggestLabel={getSuggestLabel}
          onSuggestSelect={onSuggestSelect}
          onSuggestNoResults={onSuggestNoResults}
          radius="20"
        />
        <FormHelperText>
          (Optional)The physical or non physical location of the event. Use the
          search button to find the irl location
        </FormHelperText>
      </FormControl>

      {creationError && (
        <Box mt={10}>
          <Alert status="error">
            <AlertIcon />
            <AlertTitle>{creationError.name}</AlertTitle>
            <AlertDescription>{creationError.message}</AlertDescription>
            <CloseButton
              position="absolute"
              right="2"
              top="2"
              onClick={() => {
                setCreationError(undefined);
              }}
            />
          </Alert>
        </Box>
      )}
      <Center my={10}>
        <Button isLoading={isCreating} size="lg" onClick={clickCreate}>
          Create
        </Button>
      </Center>
    </Box>
  );
}

export default Create;
