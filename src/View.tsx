import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import * as sdk from "matrix-js-sdk";
import { CustomEventType } from "./customEventTypes";
import {
  Alert,
  AlertIcon,
  AlertTitle,
  Badge,
  Box,
  Center,
  CloseButton,
  Heading,
  Spinner,
  VStack,
} from "@chakra-ui/react";

interface ViewProps {
  client: sdk.MatrixClient;
  roomId?: string;
  eventId?: string;
}

type UrlParams = {
  roomId: string;
  eventId: string;
};

function View(props: ViewProps) {
  const [title, setTitle] = useState<string>();
  const [description, setDescription] = useState<string>();
  const [start, setStart] = useState<string>();
  const [end, setEnd] = useState<string>();
  const [error, setError] = useState<string | undefined>();
  const [loading, setLoading] = useState<boolean>();
  const params = useParams<keyof UrlParams>() as UrlParams;
  const roomId: string = props.roomId || params.roomId;
  const eventId: string = props.eventId || params.eventId;

  useEffect(() => {
    async function getRoomDetails() {
      const c: sdk.MatrixClient = props.client;
      // join the room. does nothing if the room is already joined.
      await c.joinRoom(roomId);

      // check that the room has the expected room type
      const create = await c.getStateEvent(roomId, "m.room.create", "");
      if (create.type !== CustomEventType.CalendarEventRoom)
        setError(`Room ${roomId} is not a calendar event room.`);

      setLoading(true);

      // get room title
      const title: string | undefined = (
        await c.getStateEvent(roomId, "m.room.name", "")
      ).name;
      setTitle(title);

      const calendarEvent = await c.fetchRoomEvent(roomId, eventId);

      setLoading(false);

      const calenderEvent =
        calendarEvent.content[CustomEventType.CalendarEvent];
      setTitle(calenderEvent.title);
      setDescription(calenderEvent.description);
      setStart(new Date(calenderEvent.start).toLocaleString());
      setEnd(new Date(calenderEvent.end).toLocaleString());
    }

    getRoomDetails().catch(setError);
  }, [roomId, props.client, eventId]);

  return (
    <Center mt={100}>
      <VStack spacing="14px" align="start">
        <Heading size="2xl">{title}</Heading>
        <Heading size="3lg">
          <Badge>
            {start} - {end}
          </Badge>
        </Heading>
        <Heading size="3lg">{description}</Heading>
        {error && (
          <Box marginTop="6">
            <Alert status="error">
              <AlertIcon />
              <AlertTitle>{error}</AlertTitle>
              <CloseButton
                position="absolute"
                right="2"
                top="2"
                onClick={() => {
                  setError(undefined);
                }}
              />
            </Alert>
          </Box>
        )}
        {loading && (
          <Box>
            <Spinner></Spinner>
          </Box>
        )}
      </VStack>
    </Center>
  );
}

export default View;
