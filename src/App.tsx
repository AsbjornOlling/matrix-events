// deps
import { useState, useEffect } from "react";
import * as sdk from "matrix-js-sdk";
import { BrowserRouter, Routes, Route } from "react-router-dom";

// from this app
import Create from "./Create";
import Login from "./Login";
import View from "./View";
import { Nav } from "./Nav";

interface PersistedLoginData {
  baseUrl: string;
  accessToken: string;
  userId: string;
  guest: boolean;
}

function isPersistedLoginData(obj: any): obj is PersistedLoginData {
  return (
    obj.baseUrl &&
    obj.accessToken &&
    obj.userId &&
    typeof obj.guest === "boolean"
  );
}

export async function loadClient(): Promise<sdk.MatrixClient | undefined> {
  // initialize `MatrixClient` from localstorage data if available
  const unparsed: string | null = localStorage.getItem("loginData");
  if (!unparsed) return;
  const parsed: any = JSON.parse(unparsed);
  if (!isPersistedLoginData(parsed)) return;
  const client = sdk.createClient(parsed);
  client.setGuest(parsed.guest);
  await client.startClient({ disablePresence: true });
  return client;
}

export function saveClient(c: sdk.MatrixClient): void {
  // save login data to localstorage
  let data: PersistedLoginData = {
    baseUrl: c.getHomeserverUrl(),
    accessToken: c.getAccessToken(),
    userId: c.getUserId(),
    guest: c.isGuest(),
  };
  localStorage.setItem("loginData", JSON.stringify(data));
}

function App() {
  const [client, setClient] = useState<sdk.MatrixClient | undefined>(undefined);

  // check for persisted session on startup
  useEffect(() => {
    loadClient().then((c) => (c ? setClient(c) : undefined));
  }, []);

  // save login data when the client is set, clear loginData when it is not set
  useEffect(
    () => (client ? saveClient(client) : localStorage.removeItem("loginData")),
    [client]
  );

  if (!client)
    return (
      <BrowserRouter>
        <Nav setClient={setClient} client={client} />
        <Routes>
          <Route path="*" element={<Login setClient={setClient} />}></Route>
        </Routes>
      </BrowserRouter>
    );

  let createElement = <Create setClient={setClient} client={client}></Create>;
  return (
    <BrowserRouter>
      <Nav setClient={setClient} client={client} />
      <Routes>
        <Route path="/" element={<p>todo: frontpage</p>} />
        <Route path="/create" element={createElement} />
        <Route path="/login" element={<Login setClient={setClient} />} />
        <Route
          path="/event/:roomId/:eventId"
          element={<View client={client}></View>}
        />
        <Route path="*" element={"404 lol"}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
