# WIP: Matrix Events

This is an event organizing platform which builds on Matrix.

It consists of a special-purpose Matrix client written in typescript/react.
The client connects directly to a Matrix server of the users' choice, by letting them log in with their Matrix userid/password.
A user can also view public events anonymously, by registering a guest user.

It's all very WIP and doesn't really work.

## Developing

### Dependencies

In order to make full use of the development tools provided, you should have these programs installed:

- `make`
- `npm`
- `docker`
- `docker-compose`

Once you have cloned the repo, run `npm install` to install dependencies.

### Auto-reloading setup

#### With docker

There is an auto-reloading development environment in docker. You can just run `make` from the root folder of this repo to set it up.

Once you have done that, you can access the web application at `http://localhost:3000`.

#### Without docker

You can run `npm start` to run the webapp alone, without synapse.

### Testing

Since the tests depend on a synapse server to function correctly, tests need to be run in docker.

Run `make test` to run the test suite in docker-compose. A code coverage report will be output in `:/coverage`.
