# Specification

> This is a work-in-progress document describing how we might represent events
> in Matrix. This is not final. Please help improve it as we learn more :)

## Problem

We need a way to represent calendar events in Matrix. Use cases include meeting
your friends (existing group chat), inviting friends and family to your wedding
and building the next meetup platform.

## Proposal

We introduce two new event types for calendar events in Matrix; one for the
actual event and one for reporting attendance.

### Calendar event

`chat.cactus.calendar.event`: custom event for representing calendar events.
The event has four keys: `title`, `description`, `start` and `end`. `title`,
`start` and `end` are required, while `description` is optional. `start` and
`end` are UNIX epoch timestamps in UTC milliseconds. Clients that implement this proposal MUST
render this.

**TODO: verify that `start` and `end` is the same format as `origin_server_ts`.**

**TODO: include remaining metadata fields.**

`m.location`: MSC3488 location event. Clients that implement this proposal
SHOULD render this alongside the `chat.cactus.calendar.event` event.

**TODO: for many events you just want a plain text field ("The usual spot", "meet.jit.si/example.org"). We cannot account for this with MSC3488 location events.**

`m.message`: fallback extensible event text representation that must include
all information about the event. Clients that implement this proposal MUST NOT
render this.

`m.image`: optional image for the event. Clients that implement this proposal
MAY render this. **TODO: where are these defined?**

Example of a calendar event:

```json
"type": "chat.cactus.calendar.event",
"content": {
    "chat.cactus.calendar.event": {
        "title": "Hackathon at Bob's house",
        "description": "Bring your laptop!",
        "start": 1648387658838,
        "end": 1648387668838
    },
    "m.message": [
        {
            "mimetype": "text/markdown",
            "body": "# Hackathon at Bob's house\nHackathon at your buddy's place. *Bring your laptop!*\nLocation: 1337 Hacker Street\nStarts: Friday something\nEnds: Sunday something"
        },
        {
            "mimetype": "text/html",
            "body": "# Hackathon at Bob's house\nHackathon at your buddy's place. <i>Bring your laptop!</i>\nLocation: 1337 Hacker Street\nStarts: Friday something\nEnds: Sunday something"
        }
    ],
    "m.location": {
        "uri": "geo:37.786971,-122.399677;u=35",
        "description": "1337 Hacker Street"
    },
    "m.image": {
        "url": "mxc://example.org/EJWiewhIEwgJFIEWJ",
        "info": {
           "width": 512,
           "height": 512,
           "mimetype": "image/png",
           "size": 4096000
        }
    }
}
```

### Attendance event

Attendance events should use an `m.relates_to` relation to reference the calendar event id.

**TODO: Which relationship type is ideal? Polls use `m.reference`, so that might be good.**

There are three possible response options: going, not going, and maybe. These are represented by a nullable boolean, in the `attending` field.

It should use extensible events so it can fall back to a commonly-renderable event in other clients.

**TODO: Is it possible to fall back to emojji reactions? If not fall back to m.notice.**

```json
"type": "chat.cactus.calendar.response"
"content": {
    "chat.cactus.calendar.response": {
        "attending": true
    },
    "m.text": "Asbjørn is attending"
    "m.notice": {},
    "m.relates_to": {
        "rel_type": "m.reference"
        "event_id": "!boofar:cactus.chat"
    }
}
```
