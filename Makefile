dev:
	docker-compose -f dev-environment/docker-compose.yaml up --build

test:
	docker-compose -f dev-environment/docker-compose.yaml up --detach synapse
	docker-compose -f dev-environment/docker-compose.yaml run app bash -c "npm install; npm run lint; npm run test"
	docker-compose -f dev-environment/docker-compose.yaml down
