# Design

This is a very short document to describe what this app does in relation to
`spec.md` to align what we're working on.

## Scenario: you want to create an event

1. Sign-in
2. Create a new, public room
3. Send the `chat.cactus.calendar.event` event
4. Pin the calendar event

## Scenario: you get a link to an event

1. Create a guest user
2. Join room alias found in URL
3. Find event based on event id in URL
