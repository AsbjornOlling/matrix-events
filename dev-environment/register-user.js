// import * as sdk from 'matrix-js-sdk';
const sdk = require("matrix-js-sdk");

async function registerDev() {
  // wait for to synapse come up

  try {
    const client = sdk.createClient("http://localhost:8008");
    await client.register("dev", "dev", null, { type: "m.login.dummy" });
    console.log("Registered user `dev`");
  } catch (e) {
    console.log(`Failed registering dev: ${e}`);
    if (e.errcode === "M_USER_IN_USE") process.exit(0);
    registerDev();
  }
}

console.log("Starting user registration script");
registerDev();
